package com.shepherdmoney.interviewproject.controller;

import com.shepherdmoney.interviewproject.model.BalanceHistory;
import com.shepherdmoney.interviewproject.model.CreditCard;
import com.shepherdmoney.interviewproject.model.User;
import com.shepherdmoney.interviewproject.repository.CreditCardRepository;
import com.shepherdmoney.interviewproject.repository.UserRepository;
import com.shepherdmoney.interviewproject.vo.request.AddCreditCardToUserPayload;
import com.shepherdmoney.interviewproject.vo.request.UpdateBalancePayload;
import com.shepherdmoney.interviewproject.vo.response.CreditCardView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.*;
import java.util.stream.*;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
public class CreditCardController {

    // TODO: wire in CreditCard repository here (~1 line)
    @Autowired
    private CreditCardRepository creditRepository;

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/credit-card")
    public ResponseEntity<Integer> addCreditCardToUser(@RequestBody AddCreditCardToUserPayload payload) {
        // TODO: Create a credit card entity, and then associate that credit card with user with given userId
        //       Return 200 OK with the credit card id if the user exists and credit card is successfully associated with the user
        //       Return other appropriate response code for other exception cases
        //       Do not worry about validating the card number, assume card number could be any arbitrary format and length
        try{

            Optional<User> user = userRepository.findById(payload.getUserId());
            if(user.equals(Optional.empty())){
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            Set<String> creditCardNumbers = new HashSet<>();
            // Add each element of list into the set
            for (CreditCard creditCard : user.get().getCreditCards())
                creditCardNumbers.add(creditCard.getNumber());

            if (creditCardNumbers.contains(payload.getCardNumber())) {
                // Return 409 Conflict if there is already a credit card with the same number
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            }

            CreditCard newCard = new CreditCard(payload.getCardIssuanceBank(), payload.getCardNumber(), user.get());
            user.get().getCreditCards().add(newCard);
            creditRepository.save(newCard);
            return new ResponseEntity<>(newCard.getId(), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/credit-card:all")
    public ResponseEntity<List<CreditCardView>> getAllCardOfUser(@RequestParam int userId) {
        // TODO: return a list of all credit card associated with the given userId, using CreditCardView class
        //       if the user has no credit card, return empty list, never return null
        try{
            Optional<User> user = userRepository.findById(userId);
            if(user.equals(Optional.empty())){
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            List<CreditCardView> cards = user.get().getCreditCards().stream()
                    .map(card -> new CreditCardView(card.getIssuanceBank(), card.getNumber())).toList();
            return new ResponseEntity<>(cards, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/credit-card:user-id")
    public ResponseEntity<Integer> getUserIdForCreditCard(@RequestParam String creditCardNumber) {
        // TODO: Given a credit card number, efficiently find whether there is a user associated with the credit card
        //       If so, return the user id in a 200 OK response. If no such user exists, return 400 Bad Request
        try{
            Optional<CreditCard> card = creditRepository.findByNumber(creditCardNumber);
            if(card.equals(Optional.empty())){
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            return new ResponseEntity<>(card.get().getOwner().getId(), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping("/credit-card:update-balance")
    public ResponseEntity<?> postMethodName(@RequestBody UpdateBalancePayload[] payload) {
         // Assume that each payload time is at the start of the day which means we dont need to consider hour, minute, second, etc.
         //TODO: Given a list of transactions, update credit cards' balance history.
         //      For example: if today is 4/12, a credit card's balanceHistory is [{date: 4/12, balance: 110}, {date: 4/10, balance: 100}],
         //      Given a transaction of {date: 4/10, amount: 10}, the new balanceHistory is
         //      [{date: 4/12, balance: 120}, {date: 4/11, balance: 110}, {date: 4/10, balance: 110}]
         //      Return 200 OK if update is done and successful, 400 Bad Request if the given card number
         //        is not associated with a card.
         //         [
        //                {
        //                    "creditCardNumber" : "11111112",
        //                        "transactionTime" : "2023-04-21T16:31:11.329988300Z",
        //                        "transactionAmount" : 100
        //                },
        //                {
        //                    "creditCardNumber" : "11111112",
        //                        "transactionTime" : "2023-04-19T00:00:00Z",
        //                        "transactionAmount" : 100
        //                }
        //        ]
         //sort in descending time order
         Arrays.sort(payload, new Comparator<UpdateBalancePayload>() {
             @Override
             public int compare(UpdateBalancePayload lhs, UpdateBalancePayload rhs) {
                 Instant time1 = lhs.getTransactionTime();
                 Instant time2 = rhs.getTransactionTime();
                 return Integer.compare(0, time1.compareTo(time2));
             }
         });
         System.out.println(Arrays.toString(payload));
         try{
             for(UpdateBalancePayload transaction : payload){
                 // check existence
                 Optional<CreditCard> card = creditRepository.findByNumber(transaction.getCreditCardNumber());
                 if(card.equals(Optional.empty())){
                     return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                 }

                 Instant transactionDay = transaction.getTransactionTime();

                 // should we create history entries or just update. if alreadyExists, we dont need to create
                 boolean alreadyExists = card.get().getBalanceHistories().stream().anyMatch(
                         history -> history.getDate().atZone(ZoneId.systemDefault()).toLocalDate()
                                 .equals(transactionDay.atZone(ZoneId.systemDefault()).toLocalDate()));

                 // update balance values of existing entries
                 card.get().getBalanceHistories().stream()
                         .filter(history -> history.getDate().isAfter(transactionDay))
                         .forEach(history -> history.setBalance(history.getBalance() + transaction.getTransactionAmount()));

                 if(alreadyExists){
                     return new ResponseEntity<>(HttpStatus.OK);
                 }

                 // history's day not in list, create history entries
                 BalanceHistory newHistoryEntry = new BalanceHistory
                         (transaction.getTransactionTime(), transaction.getTransactionAmount());

                 if(card.get().getBalanceHistories().size() > 0){
                     // fill missing days between today and history
                     List<BalanceHistory> toAdd = new ArrayList<>();

                     BalanceHistory mostRecentHistory = card.get().getBalanceHistories().get(0);
                     BalanceHistory earlyHistory;
                     BalanceHistory lateHistory;
                     if (mostRecentHistory.getDate().compareTo(newHistoryEntry.getDate()) > 0){
                         // newHistory is earlier than mostRecentHistory
                         earlyHistory = newHistoryEntry;
                         lateHistory = mostRecentHistory;
                     }
                     else{
                         earlyHistory = mostRecentHistory;
                         lateHistory = newHistoryEntry;
                     }
                     // start from the early history and add one day at a time utill late history
                     BalanceHistory prevHistory = earlyHistory;
                     System.out.println(card.get().getBalanceHistories());
                     for(int i = 0; i < Duration.between(earlyHistory.getDate(), lateHistory.getDate()).toDays(); i++){
                         BalanceHistory newHistory = new BalanceHistory(
                                 prevHistory.getDate().plus(24, ChronoUnit.HOURS), prevHistory.getBalance());
                         System.out.println(newHistory);
                         boolean existsEntry = card.get().getBalanceHistories().stream().anyMatch(
                                 history -> history.getDate().atZone(ZoneId.systemDefault()).toLocalDate()
                                         .equals(newHistory.getDate().atZone(ZoneId.systemDefault()).toLocalDate()));
                         if(!existsEntry){
                             System.out.println("history not exists");
                             toAdd.add(newHistory);
                         }
                         prevHistory = newHistory;
                     }

                     card.get().getBalanceHistories().addAll(toAdd);
                     creditRepository.save(card.get());
                 }
                 else{
                     System.out.println("first history entry, no balance history before");
                     card.get().getBalanceHistories().add(newHistoryEntry);
                     creditRepository.save(card.get());
                 }
             }

             return new ResponseEntity<>(HttpStatus.OK);
         }
         catch (Exception e) {
             HttpHeaders responseHeader = new HttpHeaders();
             responseHeader.add("error", e.toString());
             return new ResponseEntity<>(responseHeader, HttpStatus.INTERNAL_SERVER_ERROR);
         }
    }
    
}
