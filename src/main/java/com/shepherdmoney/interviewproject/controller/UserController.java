package com.shepherdmoney.interviewproject.controller;

import com.shepherdmoney.interviewproject.model.User;
import com.shepherdmoney.interviewproject.repository.UserRepository;
import com.shepherdmoney.interviewproject.vo.request.CreateUserPayload;

import jakarta.xml.ws.spi.http.HttpHandler;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    // TODO: wire in the user repository (~ 1 line)
    @Autowired
    private UserRepository userRepository;
    
    @PutMapping("/user")
    public ResponseEntity<Integer> createUser(@RequestBody CreateUserPayload payload) {
        // TODO: Create an user entity with information given in the payload, store it in the database
        //       and return the id of the user in 200 OK response
        try{
            User newUser = new User(payload.getName(), payload.getEmail());
            userRepository.save(newUser);
            return new ResponseEntity<>(newUser.getId(), HttpStatus.OK);
        }
        catch (Exception e) {
            HttpHeaders responseHeader = new HttpHeaders();
            responseHeader.add("error", e.toString());
            return new ResponseEntity<>(responseHeader, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping("/user")
    public ResponseEntity<String> deleteUser(@RequestParam int userId) {
        // TODO: Return 200 OK if a user with the given ID exists, and the deletion is successful
        //       Return 400 Bad Request if a user with the ID does not exist
        //       The response body could be anything you consider appropriate

        try{
            if(!userRepository.findById(userId).equals(Optional.empty())){
                userRepository.deleteById(userId);
                return new ResponseEntity<>("Succsssfully delete", HttpStatus.OK);
            }
            return new ResponseEntity<>("Cant find user", HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
