package com.shepherdmoney.interviewproject.vo.request;

import java.time.Instant;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UpdateBalancePayload {

    private String creditCardNumber;
    
    private Instant transactionTime;

    private double transactionAmount;
}
