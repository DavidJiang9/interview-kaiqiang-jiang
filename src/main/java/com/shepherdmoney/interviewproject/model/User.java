package com.shepherdmoney.interviewproject.model;

import java.util.List;

import io.micrometer.common.lang.NonNull;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@NoArgsConstructor(force = true)
@Table(name = "MyUser")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @lombok.NonNull
    private String name;
    @lombok.NonNull
    private String email;

    // TODO: User's credit card
    // HINT: A user can have one or more, or none at all. We want to be able to query credit cards by user
    //       and user by a credit card.
    @OneToMany(cascade = CascadeType.ALL)
    private List<CreditCard> creditCards;
}
